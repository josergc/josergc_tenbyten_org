package josergc;

/**
 * 
 * @author josergc@gmail.com
 *
 */
public interface TenByTen_Org_Listener {

	public void connectingWithSite();

	public void retrievingData();

	public void progress(int i, int maxWords);

	public void downloadSuccess();

	public void downloadThumbnailCompleted(int i);

	public void retrievingThumbnailData(int i);

	public void retrievingImageData(int i);

	public void downloadImageCompleted(int i);

}
