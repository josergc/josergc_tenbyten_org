package josergc;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * 
 * @author josergc@gmail.com
 *
 */
public class TenByTen_Org extends MIDlet {

    private Display display;
	protected TenByTen_Org_Model ctrl;
	private TenByTen_Org_Texts texts;

    public void startApp() throws MIDletStateChangeException {
        display = Display.getDisplay(this);
        InputStream is;
		try {
			is = resolveLocale();
		} catch (Throwable e) {
			is = null;
		}
		Properties properties;
		try {
			properties = new Properties(is == null ? new ByteArrayInputStream(new byte[0],0,0) : is);
		} catch(Throwable e){
			properties = new Properties();
		}
        texts = new TenByTen_Org_Texts(properties);
        loadDefault();
    }

    private static InputStream resolveLocale() throws IOException {
    	String locale = System.getProperty("microedition.locale");
    	InputStream is = resolveResource("/" + locale + ".txt");
    	return is == null ?
    			resolveResource("/en-EN.txt")
    		:
    			is
    		;
	}

	private static InputStream resolveResource(String string) throws IOException {
		InputStream is = TenByTen_Org.class.getResourceAsStream(string);
		if (is == null) {
			is = TenByTen_Org.class.getResourceAsStream(string + ".lnk");
			if (is != null) {
				final int total = is.available();
				byte[] ba = new byte[total];
				int nBytes, offset = 0;
				while ((nBytes = is.read(ba,offset,total - offset)) > 0) {
					offset += nBytes;
				}
				return TenByTen_Org.class.getResourceAsStream(new String(ba,0,total,"UTF8"));
			}
		}
		return is;
	}

	private void loadDefault() {
		Thread thread = new Thread() { public void run() {
			try {
				Alert alert = new Alert(texts.loading()); 
				Displayable displayable = display.getCurrent();
				ctrl = TenByTen_Org_Controller.getNow(new TenByTen_Org_Listener() {
					public void retrievingThumbnailData(int i) {
						// TODO Auto-generated method stub
					}
					public void retrievingImageData(int i) {
						// TODO Auto-generated method stub
					}
					public void retrievingData() {
						// TODO Auto-generated method stub
					}
					public void progress(int i, int maxWords) {
						// TODO Auto-generated method stub
					}
					public void downloadThumbnailCompleted(int i) {
						// TODO Auto-generated method stub
					}
					public void downloadSuccess() {
						// TODO Auto-generated method stub
					}
					public void downloadImageCompleted(int i) {
						// TODO Auto-generated method stub
					}
					public void connectingWithSite() {
					}
				});
			} catch (ConnectionNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}};
		thread.start();
	}

	public void pauseApp() {
    	notifyPaused();
    }

    public void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        notifyDestroyed();
    }

}
