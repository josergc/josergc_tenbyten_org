package josergc;

import java.io.IOException;

import javax.microedition.lcdui.Image;

/**
 * <p>Reference:</p>
 * <p><a href="http://www.tenbyten.org/developers.html">Developer? 10x10 is here for you.</a></p>
 * @author josergc@gmail.com
 *
 */
public interface TenByTen_Org_Model {
	/**
	 * 10 x 10 = 100 words
	 */
	public static final int MAX_WORDS = 100;
	public static final int THUMBNAIL_IMAGE_WIDTH = 60;
	public static final int THUMBNAIL_IMAGE_HEIGHT = 40;
	public static final int IMAGE_WIDTH = 227;
	public static final int IMAGE_HEIGHT = 149;
	/**
	 * Returns an array of {@link #MAX_WORDS} words
	 * @return array of {@link String}
	 * @throws IOException
	 */
	public String[] getWords() throws IOException;
	/**
	 * Returns an array of thumbnail {@link Image}, if the image is null, it 
	 * will be still loading 
	 * @return array of {@link Image}
	 * @throws IOException
	 */
	public Image[] getThumbnails() throws IOException;
	/**
	 * Retrieves the image for the given index
	 * @param i
	 * @return
	 * @throws IOException
	 */
	public Image getImage(int i) throws IOException;
}
