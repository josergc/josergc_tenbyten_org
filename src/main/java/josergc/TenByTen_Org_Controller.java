package josergc;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Vector;

import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * 
 * @author josergc@gmail.com
 *
 */
public class TenByTen_Org_Controller {

	/**
	 * Tries to get an instance of the {@link TenByTen_Org_Model} using the 
	 * current date-time or an approximated
	 * @param l
	 * @return
	 * @throws ConnectionNotFoundException if there is a problem with the server
	 * @throws IOException
	 */
	public static TenByTen_Org_Model getNow(TenByTen_Org_Listener l) throws ConnectionNotFoundException, IOException {
		TimeZone tz = TimeZone.getTimeZone("EST");
		Calendar calendar = Calendar.getInstance(tz);
		if ("GMT".equals(tz.getID())) {
			calendar.setTime(new Date(calendar.getTime().getTime() - 18000000L));
		}
		ConnectionNotFoundException lastConnectionNotFoundException = null;
		for (int i = 0; i < 5; i++) {
			try {
				TenByTen_Org_Model model = get(
					calendar.get(Calendar.YEAR),
					calendar.get(Calendar.MONTH) + 1, // Month correction
					calendar.get(Calendar.DAY_OF_MONTH),
					calendar.get(Calendar.HOUR_OF_DAY),
					l
					);
				model.getWords();
				return model;
			} catch(ConnectionNotFoundException e) {
				// Try with one less hour
				lastConnectionNotFoundException = e;
				calendar.setTime(new Date(calendar.getTime().getTime() - 3600000L));
			}
		}
		if (lastConnectionNotFoundException == null) {
			lastConnectionNotFoundException = new ConnectionNotFoundException("TenByTen_Org_Controller: Internal error");
		}
		throw lastConnectionNotFoundException;
	}

	protected static String platformDependentSuffix;
	static {
		String suffix;
		try {
			final HttpConnection c = (HttpConnection)Connector.open("http://tenbyten.org/");
			try {
				c.openInputStream().close();
			} finally {
				c.close();
			}
			suffix = new String();
		} catch(Throwable e) {
			try {
				Class.forName("net.rim.device.api.ui.Color");
				suffix = ";interface=wifi";
			} catch(Throwable e1) {
				suffix = new String();
			}
		}
		platformDependentSuffix = suffix;
	}

	/**
	 * Retrieves the model for the given date-time. Use -1 to skip that field
	 * @param year
	 * @param month from 1 to 12
	 * @param day
	 * @param hour
	 * @param l this can't be null
	 * @return an instance of {@link TenByTen_Org_Model}
	 * @throws NullPointerException if 'l' is null
	 */
	public static TenByTen_Org_Model get(final int year,final int month,final int day,final int hour,final TenByTen_Org_Listener l) {
		StringBuffer sb = new StringBuffer().append("http://tenbyten.org/Data/global/");
		if (year != -1) {
			sb.append(toStringWithLFill(year,4)).append('/');
			if (month != -1) {
				sb.append(toStringWithLFill(month,2)).append('/');
				if (day != -1) {
					sb.append(toStringWithLFill(day,2)).append('/');
					if (hour != -1) {
						sb.append(toStringWithLFill(hour,2)).append('/');
					}
				}
			}
		}
		final String URL = sb.toString();
		
		return new TenByTen_Org_Model() {
			private String[] words;
			private Image[] thumbnails;
			private final Vector threads = new Vector();

			public String[] getWords() throws IOException {
				if (words == null) {
					l.connectingWithSite();
					final HttpConnection c = (HttpConnection)Connector.open(URL + "words.txt" + platformDependentSuffix);
					try {
						l.retrievingData();
						final InputStream is = c.openInputStream();
						try { 
							final InputStreamReader isr = new InputStreamReader(is);
							final String[] sa = new String[TenByTen_Org_Model.MAX_WORDS];
							int i = 0;
							int ch;
							StringBuffer sb = new StringBuffer();
							while ((ch = isr.read()) != -1 && i < TenByTen_Org_Model.MAX_WORDS) {
								if (ch == '\n') {
									String s = sb.toString();
									sa[i++] = s;
									sb.delete(0,s.length());
									l.progress(i,TenByTen_Org_Model.MAX_WORDS);
								} else {
									sb.append((char)ch);
								}
							}
							sa[i] = sb.toString();
							l.downloadSuccess();
							return words = sa;
						} finally {
							is.close();
						}
					} finally {
						c.close();
					}
				}
				return words;
			}

			public Image[] getThumbnails() throws IOException {
				if (thumbnails == null) {
					final String[] words = getWords();
					final int wordsLength = words.length;
					final Image[] ia = thumbnails = new Image[wordsLength];
					final Thread thread = new Thread() { public void run() {
						for (int iImage = 0; iImage < wordsLength; iImage++) {
							l.connectingWithSite();
							try {
								final HttpConnection c = (HttpConnection)Connector.open(URL + words[iImage] + "2.jpg" + platformDependentSuffix);
								try {
									l.retrievingThumbnailData(iImage);
									final InputStream is = c.openInputStream();
									try { 
										ia[iImage] = Image.createImage(is);
									} finally {
										is.close();
									}
								} finally {
									c.close();
								}
							} catch(Throwable e) {
								// Alternative image for when the connection fails
								ia[iImage] = brokenImage(Image.createImage(
									TenByTen_Org_Model.THUMBNAIL_IMAGE_WIDTH,
									TenByTen_Org_Model.THUMBNAIL_IMAGE_HEIGHT
									));
							}
							l.downloadThumbnailCompleted(iImage);
							threads.removeElement(this);
						}
					}

					};
					synchronized(threads) {
						threads.addElement(thread);
					}
					thread.start();
				}
				return thumbnails;
			}

			public Image getImage(int i) throws IOException {
				l.connectingWithSite();
				Image image;
				try {
					final HttpConnection c = (HttpConnection)Connector.open(URL + words[i] + ".jpg" + platformDependentSuffix);
					try {
						l.retrievingImageData(i);
						final InputStream is = c.openInputStream();
						try { 
							image = Image.createImage(is);
						} finally {
							is.close();
						}
					} finally {
						c.close();
					}
				} catch(Throwable e) {
					// Alternative image for when the connection fails
					image = brokenImage(Image.createImage(
						TenByTen_Org_Model.THUMBNAIL_IMAGE_WIDTH,
						TenByTen_Org_Model.THUMBNAIL_IMAGE_HEIGHT
						));
				}
				l.downloadImageCompleted(i);
				return image;
			}
			};
	}

	/**
	 * Paints a red cross on box of the given {@link Image}
	 * @param image
	 * @return the same given image object
	 */
	private static Image brokenImage(Image image) {
		final int width = image.getWidth();
		final int height = image.getHeight();
		Graphics g = image.getGraphics();
		g.setColor(0xff0000);
		g.drawLine(0,0,width,height);
		g.drawLine(0,height,width,0);
		g.drawRect(0,0,width - 1,height - 1);
		return image;
	}
	
	/**
	 * 
	 * @param value
	 * @param minLength
	 * @return
	 */
	private static String toStringWithLFill(int value, int minLength) {
		final String s = Integer.toString(value);
		final int sLength = s.length();
		if (sLength < minLength) {
			StringBuffer sb = new StringBuffer();
			for (int i = sLength; i < minLength; i++) {
				sb.append('0');
			}
			return sb.append(s).toString();
		}
		return s;
	}
}
