package josergc;

import java.io.InputStream;

import javax.microedition.midlet.MIDlet;

public class TenByTen_Org_Texts {

	private Properties properties;

	public TenByTen_Org_Texts(Properties properties) {
		this.properties = properties;
	}

	public String loading() {
		return properties.getText("Loading","Loading...");
	}

}
